FILESEXTRAPATHS:prepend:raspberrypi3-64 := "${THISDIR}/files:"

#SRC_URI:stm32mp1 = "file://boot.scr.cmd"
SRC_URI:raspberrypi3-64 += " file://${MACHINE}/boot.cmd.in "

do_compile() {
    sed -e 's/@@KERNEL_IMAGETYPE@@/${KERNEL_IMAGETYPE}/' \
        -e 's/@@KERNEL_BOOTCMD@@/${KERNEL_BOOTCMD}/' \
        "${WORKDIR}/${MACHINE}/boot.cmd.in" > "${WORKDIR}/boot.cmd" 
    mkimage -A ${UBOOT_ARCH} -T script -C none -n "Boot script" -d "${WORKDIR}/boot.cmd" boot.scr
}
