#This line is to reboot the system when a kernel panic occurs
CMDLINE_ROOTFS = "${CMDLINE_ROOT_FSTYPE} rootwait"
CMDLINE += " panic=3 "

# 3 is for 3 seconds (reboot after 3 seconds)