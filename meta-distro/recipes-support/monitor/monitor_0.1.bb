SUMMARY = "Monitoring service for RWS"
DESCRIPTION = "A tool to log various events related to processes, memory usage, or CPU use."
SECTION = "rws"

LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${S}/LICENSE.txt;md5=b9657b1ee23f2bf9f457b8a756b82b30"

DEPENDS += " \
    systemd \
    libyaml \
"

SRC_URI = "git://gitlab.com/embedded_linux_factory/monitor.git;protocol=https;branch=main"
SRC_URI += " file://monitor.service "
SRCREV = "0279ef288fd15057522fedf2a37647a1d3fb079b"

S = "${WORKDIR}/git"

# By default, add DLT option
PACKAGECONFIG ?= " dlt "

# Logging options
PACKAGECONFIG[dlt] = "-Dlog_system=dlt,-Dlog_system=journalctl,dlt-daemon,dlt-daemon"

inherit meson pkgconfig systemd features_check

do_install:append() {

    # Install unit file
    install -d "${D}${systemd_unitdir}/system"
    install -m 0644 "${WORKDIR}/monitor.service" "${D}${systemd_unitdir}/system"
        
    install -d "${D}${sysconfdir}/monitord"
    install -m 0644 "${S}/config/generic.yaml" "${D}${sysconfdir}/monitord"

}

REQUIRED_DISTRO_FEATURES += "systemd"

SYSTEMD_SERVICE:${PN} = "monitor.service"

FILES:${PN} += " ${systemd_unitdir}/system/monitor.service "

RDEPENDS:${PN} += " \
    systemd \
    libyaml \
"

