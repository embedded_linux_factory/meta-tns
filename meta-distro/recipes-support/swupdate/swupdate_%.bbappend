FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

#SRC_URI += " file://swupdate.service \
#    file://swupdate.socket \
#"
SRC_URI += " file://09-swupdate-args \
    file://swupdate.cfg \
    file://defconfig \
"


#SWUPDATE_SOCKET_CTRL_PATH:prepend = "/tmp/sockinstctrl"
#SWUPDATE_SOCKET_PROGRESS_PATH:prepend = "/tmp/swupdateprog"

SWUPDATE_HW_REVISION_ETC = "1.0"


do_install:append() {
    install -d ${D}${libdir}
    install -d ${D}${libdir}/swupdate
    install -d ${D}${libdir}/swupdate/conf.d/
    # 1.0 should be replaced by a generic variable when we have a lot of types of hardware
    sed -e 's/@@MACHINE@@/${MACHINE}/' \
    -e 's/@@SWUPDATE_HW_REVISION@@/1.0/' \
    "${WORKDIR}/09-swupdate-args" >> "${D}${libdir}/swupdate/conf.d/09-swupdate-args"

    install -d ${D}/etc
    echo "${MACHINE} 1.0" >> "${D}${sysconfdir}/hwrevision"

    install -d ${D}${sysconfdir}
    install -m 644 ${WORKDIR}/swupdate.cfg ${D}${sysconfdir}

    echo "/boot/uboot.env 0x0000    0x4000" >> ${D}${sysconfdir}/fw_env.config
}
#do_install:append:stm32mp1() {

#    install -d ${D}/lib/systemd/system
#    install -m 644 ${WORKDIR}/swupdate.service ${D}${systemd_system_unitdir}
#    install -m 644 ${WORKDIR}/swupdate.socket ${D}${systemd_system_unitdir}
#}

#do_install:append:stm32mp1() {
#    echo "/dev/mmcblk0p7    -0x2000 0x2000" > ${D}${sysconfdir}/fw_env.config
#    echo "/dev/mmcblk0p7    -0x2000 0x2000" >> ${D}${sysconfdir}/fw_env.config
#}

#do_install:append:raspberrypi3-64() {
#    echo "/boot/uboot.env 0x0000    0x4000" > ${D}${sysconfdir}/fw_env.config
#}

FILES:${PN}-www += " \
    ${sysconfdir}/fw_env.config \
    ${sysconfdir}/swupdate.cfg \
    ${sysconfdir}/hwrevision \
    ${libdir}/swupdate/conf.d/09-swupdate-args \
"
