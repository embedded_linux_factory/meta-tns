FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://10-static-eth0.network"

do_install:append () {
    install -m 644 ${WORKDIR}/10-static-eth0.network ${D}/etc/systemd/network/
}