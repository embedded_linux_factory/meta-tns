#!/bin/sh

if [ $# -lt 1 ]; then
    exit 0;
fi

mount_by_label() {
    LABEL=$1
    mkdir /mnt/$LABEL
    mount /dev/mmcblk0p5 /mnt/$LABEL/
}

unmount_by_label() {
    LABEL=$1
    umount /mnt/$LABEL/
    rmdir /mnt/$LABEL
}

if [ $1 == "preinst" ]; then
    echo "update preinst"
fi

if [ $1 == "postinst" ]; then
    echo "update postinst"
fi 