SUMMARY = "A small image just capable of allowing a device to boot and run swupdate, including monitor."

IMAGE_INSTALL:append = " monitor "


IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL}"

inherit core-image

LICENSE = "MIT"

IMAGE_INSTALL:append = " gdb gdbserver   \
                         u-boot-fw-utils libubootenv \
                         swupdate swupdate-www \
                         swupdate-tools \
                         strace ltrace valgrind \
                         util-linux procps \
                         "


IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE:append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "", d)}"
IMAGE_FSTYPES:raspberrypi3-64 = "rpi-sdimg ext4.gz"

python () {

    #CASE QEMU
    if d.getVar('MACHINE', True) == "qemux86-64":
        d.setVarFlag('SWUPDATE_IMAGES_FSTYPES', d.getVar('IMAGE_LINK_NAME', True), ".tar.bz2")

    #CASE RASPBERRY3
    if d.getVar('MACHINE', True) == "raspberrypi3-64":
        d.setVarFlag('SWUPDATE_IMAGES_FSTYPES', d.getVar('IMAGE_LINK_NAME', True), ".rpi-sdimg")

    #CASE STM32MP1
    elif d.getVar('MACHINE', True) == "stm32mp1":
        d.setVarFlag('SWUPDATE_IMAGES_FSTYPES', d.getVar('IMAGE_LINK_NAME', True), ".ext4")
        # =========================================================================
        # Set features for STM32MP157F-DK2
        # =========================================================================
        # Disable vendorfs and userfs
        d.setVar("ST_BOOTFS"   ,"1")
        d.setVar("ST_VENDORFS" ,"0")
        d.setVar("ST_USERFS"   ,"0")

        # Acceptance of the EULA to have acces to all stm features
        d.setVar("ACCEPT_EULA_stm32mp1" ,"1")

        # Define specific board reference to use
        d.setVar("M4_BOARDS" ,"STM32MP157F-DK2")
}

WKS_IMAGE_FSTYPES += "wic wic.bz2 wic.bmap"
WKS_FILE += "${MACHINE}-kirkstone-dualroot.wks.in"
