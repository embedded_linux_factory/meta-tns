FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = "  \
        file://dlt-dbus.conf \
        file://dlt.conf \
        file://dlt-system.conf \
"
PACKAGECONFIG = "	\
				${@bb.utils.contains('DISTRO_FEATURES', 'systemd', ' systemd systemd-watchdog systemd-journal ', '', d)} \
				dlt-system dlt-dbus dlt-filetransfer \
"

do_install:append(){
    install -m 0644 ${WORKDIR}/dlt.conf ${D}${sysconfdir}/
    install -m 0644 ${WORKDIR}/dlt-system.conf ${D}${sysconfdir}/
    install -m 0644 ${WORKDIR}/dlt-dbus.conf ${D}${sysconfdir}/
}
